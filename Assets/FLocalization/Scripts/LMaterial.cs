﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FLocalization
{
    public class LMaterial : MonoBehaviour
    {
        public string id;

        public int index = 0; 

        private Renderer renderer;

        private void Awake()
        {
            Localization.Register(this);
        }

        private void OnEnable()
        {
            UpdateMaterial();
        }

        public void UpdateMaterial()
        {
            renderer = GetComponent<Renderer>();

            index = Mathf.Clamp(index, 0, renderer.materials.Length - 1);

            Material[] materials = renderer.materials;

            materials[index] = Localization.GetMaterial(id);

            renderer.materials = materials;
        }

        private void OnDestroy()
        {
            Localization.Unregister(this);
        }

        private void OnValidate()
        {
            name = id;
        }
    }
}
