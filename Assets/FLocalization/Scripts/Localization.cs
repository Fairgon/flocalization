﻿using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using FLocalization.FEditor;

namespace FLocalization
{
    public class Localization : MonoBehaviour
    {
        public static Languages localization;

        public static List<Inscription> inscriptions = new List<Inscription>();
        public static List<LSprite> sprites = new List<LSprite>();
        public static List<LMaterial> materials = new List<LMaterial>();

        public static string Language = "English";

        private void Awake()
        {
            localization = Resources.Load<Languages>("FLocalization/Localization");
        }

        private void Start()
        {
            Translate();
        }

        public static void Register(Inscription inscription)
        {
            if (!inscriptions.Contains(inscription))
                inscriptions.Add(inscription);
        }

        public static void Unregister(Inscription inscription)
        {
            inscriptions.Remove(inscription);
        }

        public static void Register(LSprite sprite)
        {
            if (!sprites.Contains(sprite))
                sprites.Add(sprite);
        }

        public static void Unregister(LSprite sprite)
        {
            sprites.Remove(sprite);
        }

        public static void Register(LMaterial material)
        {
            if (!materials.Contains(material))
                materials.Add(material);
        }

        public static void Unregister(LMaterial material)
        {
            materials.Remove(material);
        }

        /// <summary>
        /// Translate all.
        /// </summary>
        public static void Translate()
        {
            foreach (Inscription inscription in inscriptions)
            {
                inscription.UpdateInscription();
            }

            foreach (LSprite sprite in sprites)
            {
                sprite.UpdateSprite();
            }

            foreach (LMaterial material in materials)
            {
                material.UpdateMaterial();
            }
        }
        
        public static string GetString(string name)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Element element = language.GetElementByTag(name);
                    
                    if (element == null) return "";
                    else return element.Value;
                }
            }

            return "";
        }

        public static string GetString(int id)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    return language.GetElementById(id).Value;
                }
            }

            return "";
        }

        public static Sprite GetSprite(string name)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementByTag(name).obj;

                    if (obj != null) return obj as Sprite;

                    break;
                }
            }

            return Sprite.Create(Texture2D.blackTexture, new Rect(0, 0, 2, 2), Vector2.zero);
        }

        public static Sprite GetSprite(int id)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementById(id).obj;

                    if (obj != null) return obj as Sprite;

                    break;
                }
            }
            
            return Sprite.Create(Texture2D.blackTexture, new Rect(0, 0, 2, 2), Vector2.zero);
        }

        public static AudioSource GetAudio(string name)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementByTag(name).obj;

                    if (obj != null) return obj as AudioSource;

                    break;
                }
            }

            return new AudioSource();
        }

        public static AudioSource GetAudio(int id)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementById(id).obj;

                    if (obj != null) return obj as AudioSource;

                    break;
                }
            }

            return new AudioSource();
        }

        public static Texture2D GetTexture(string name)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementByTag(name).obj;

                    if (obj != null) return obj as Texture2D;

                    break;
                }
            }

            return Texture2D.blackTexture;
        }

        public static Texture2D GetTexture(int id)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementById(id).obj;

                    if (obj != null) return obj as Texture2D;

                    break;
                }
            }

            return Texture2D.blackTexture;
        }
        
        public static Material GetMaterial(string name)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementByTag(name).obj;

                    if (obj != null) return obj as Material;

                    break;
                }
            }

            return null;
        }

        public static Material GetMaterial(int id)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementById(id).obj;

                    if (obj != null) return obj as Material;

                    break;
                }
            }

            return null;
        }

        public static GameObject GetPrefab(string name)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementByTag(name).obj;

                    if (obj != null) return obj as GameObject;

                    break;
                }
            }

            return new GameObject();
        }

        public static GameObject GetPrefab(int id)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementById(id).obj;

                    if (obj != null) return obj as GameObject;

                    break;
                }
            }

            return new GameObject();
        }

        public static Object GetObject(string name)
        {
            List<Language> languages = localization.languages;

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementByTag(name).obj;

                    if (obj != null) return obj;

                    break;
                }
            }

            return new Object();
        }

        public static Object GetObject(int id)
        {
            List<Language> languages = localization.languages; 

            foreach (Language language in languages)
            {
                if (Language == language.Name)
                {
                    Object obj = language.GetElementById(id).obj;

                    if (obj != null) return obj;

                    break;
                }
            }

            return new Object();
        }
    }
}

