﻿using UnityEngine;
using System.Collections;
using TMPro;

namespace FLocalization
{
    public class InscriptionTMP : Inscription
    {
        private TextMeshPro Title;

        public override void UpdateInscription()
        {
            if (GetComponent<TextMeshPro>())
                Title = GetComponent<TextMeshPro>();
            else if (transform.GetComponentInChildren<TextMeshPro>())
                Title = transform.Find("Text").GetComponent<TextMeshPro>();

            Title.text = prefix + Localization.GetString(id) + postfix;
        }
    }
}
