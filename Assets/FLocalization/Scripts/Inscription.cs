﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FLocalization
{
    public class Inscription : MonoBehaviour
    {
        public string id;

        public string prefix;
        public string postfix;

        private Text Title;

        private void Awake()
        {
            Localization.Register(this);
        }

        private void OnEnable()
        {
            UpdateInscription();
        }

        public virtual void UpdateInscription()
        {
            if (GetComponent<Text>())
                Title = GetComponent<Text>();
            else if (transform.GetComponentInChildren<Text>())
                Title = transform.Find("Text").GetComponent<Text>();

            Title.text = prefix + Localization.GetString(id) + postfix;
        }

        private void OnDestroy()
        {
            Localization.Unregister(this);
        }

        private void OnValidate()
        {
            name = id;
        }
    }
}


