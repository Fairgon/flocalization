﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Xml;

namespace FLocalization.FEditor
{
    public class LocalizationWindow : EditorWindow
    {
        private static Vector2 scroolPos;
        
        private int languageIndex = 0;

        private int sliderIndex = 0;

        private string searchString = "";
        private ElementType sortMask = (ElementType)(-1);
        
        private string[] types = new string[] { "Everything", "Text", "Sprite", "Audio", "Texture", "Material", "Preafb", "Object" };

        private int selectedType = 0;

        public static GUIStyle bS;
        public static GUIStyle bL;
        public static GUIStyle bM;
        public static GUIStyle bR;
        
        [MenuItem("Window/FLocalization")]
        public static void ShowWindow()
        {
            EditorWindow window = GetWindow(typeof(LocalizationWindow));

            window.titleContent = new GUIContent("FLocalization");
            window.minSize = new Vector2(820, 400);

            LocalizationManager.Load();
        }

        private void OnEnable()
        {
            LocalizationManager.Load();
        }

        private static void FixAlignment(params GUIStyle[] styles)
        {
            foreach (GUIStyle style in styles)
            {
                style.padding = new RectOffset(2, 2, 2, 2);
            }
        }
        
        private void OnGUI()
        {
            GUIStyle titleStyle = new GUIStyle { alignment = TextAnchor.MiddleCenter, fontStyle = FontStyle.Bold, wordWrap = true, fontSize = 15 };
            GUIStyle textStyle = new GUIStyle { alignment = TextAnchor.MiddleCenter, fontStyle = FontStyle.Bold, richText = true };
            
            bS = new GUIStyle(EditorStyles.miniButton);
            bL = new GUIStyle(EditorStyles.miniButtonLeft);
            bM = new GUIStyle(EditorStyles.miniButtonMid);
            bR = new GUIStyle(EditorStyles.miniButtonRight);
            
            FixAlignment(bS, bL, bM, bR);

            GUILayout.BeginArea(new Rect(10, 0, this.position.width - 20, 45), EditorStyles.helpBox);

            EditorGUI.LabelField(new Rect(0, 0, this.position.width, 45), "Localization Manager", titleStyle);
            
            GUILayout.EndArea();
            
            GUILayout.BeginArea(new Rect(10, 45, this.position.width - 20, 30), EditorStyles.helpBox);

            if (GUI.Button(new Rect(10, 2.5f, 25, 25), GetSaveIcon(), bS)) Save();

            GUI.Box(new Rect(45, 5, 5, 20), "", EditorStyles.helpBox);

            languageIndex = EditorGUI.Popup(new Rect(60, 7.5f, 150, 16), languageIndex, GetLanguagesName().ToArray());
            
            if (GUI.Button(new Rect(220, 5, 20, 20), LoadIcon("Add", "Add language"), bL))
                AddNewLanguage();

            if (GUI.Button(new Rect(240, 5, 20, 20), GetDefaultIcon(), bR))
                LocalizationManager.SetDefault(LocalizationManager.current.Name);

            SetCurrentLanguage();

            GUI.Box(new Rect(270, 5, 5, 20), "", EditorStyles.helpBox);

            if (GUI.Button(new Rect(285, 2.5f, 25, 25), LoadIcon("Text", "Add text element"), bL)) CreateNewElement(ElementType.text);
            if (GUI.Button(new Rect(310, 2.5f, 25, 25), LoadIcon("Sprite", "Add sprite element"), bM)) CreateNewElement(ElementType.sprite);
            if (GUI.Button(new Rect(335, 2.5f, 25, 25), LoadIcon("Audio", "Add audio element"), bM)) CreateNewElement(ElementType.audio);
            if (GUI.Button(new Rect(360, 2.5f, 25, 25), LoadIcon("Texture", "Add texture element"), bM)) CreateNewElement(ElementType.texture);
            if (GUI.Button(new Rect(385, 2.5f, 25, 25), LoadIcon("Material", "Add material element"), bM)) CreateNewElement(ElementType.material);
            if (GUI.Button(new Rect(410, 2.5f, 25, 25), LoadIcon("Prefab", "Add prefab element"), bM)) CreateNewElement(ElementType.prefab);
            if (GUI.Button(new Rect(435, 2.5f, 25, 25), LoadIcon("Object", "Add object element"), bR)) CreateNewElement(ElementType.obj);
            
            searchString = GUI.TextField(new Rect(this.position.width - 295, 7.5f, 150, 16), searchString, 40);

            selectedType = EditorGUI.Popup(new Rect(this.position.width - 145, 7.5f, 15, 16), "", selectedType, types, EditorStyles.textArea);
            sortMask = (ElementType)selectedType;
            
            GUI.Box(new Rect(this.position.width - 120, 5, 5, 20), "", EditorStyles.helpBox);

            if (GUI.Button(new Rect(this.position.width - 105, 2.5f, 25, 25), LoadIcon("Script", "Generate a script of constant"), bL)) ConstantsGeneratorWindow.ShowWindow();
            if (GUI.Button(new Rect(this.position.width - 80, 2.5f, 25, 25), LoadIcon("Import", "Import"), bM)) FileManager.Import();
            if (GUI.Button(new Rect(this.position.width - 55, 2.5f, 25, 25), LoadIcon("Export", "Export"), bR)) FileManager.Export();

            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(10, 75, this.position.width - 20, this.position.height - 90), EditorStyles.helpBox);

            GUILayout.BeginArea(new Rect(5, 10, this.position.width - 30, 30), EditorStyles.helpBox);

            GUI.Label(new Rect(10, 0, 80, 30), "Type", textStyle);
            GUI.Label(new Rect(100, 0, 60, 30), "Id", textStyle);
            GUI.Label(new Rect(170, 0, 200, 30), "Name", textStyle);
            GUI.Label(new Rect(380, 0, this.position.width - 520, 30), "Value", textStyle);

            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(0, 45, this.position.width - 25, this.position.height - 135));

            int counter = 0;
            int height = 25;
            int defHeight = 25;

            int offset = 0;

            int availableHeight = (int)(this.position.height - 140);
            int elementsHeight = 0;

            //int elementsPerPage = (int)(availableHeight / defHeight);
            int maxScrollValue = 0;

            List<Element> elements = LocalizationManager.GetElements();

            //sliderIndex = Mathf.Clamp(sliderIndex, 0, elements.Count - 1);

            for (int i = 0; i < elements.Count; ++i)
            {
                Element element = elements[i];

                height = defHeight;
                GUIContent content = new GUIContent(element.Value);
                float factor = EditorStyles.textArea.CalcHeight(content, this.position.width - 480) / 17;

                if (elementsHeight < availableHeight)
                {
                    elementsHeight += defHeight + (int)(16 * (factor - 1));

                    maxScrollValue++;
                }
                else break;
            }

            if (Event.current.type == EventType.ScrollWheel && Mathf.Abs(Event.current.delta.y) > 0)
            {
                sliderIndex += (Event.current.delta.y > 0) ? 1 : -1;

                GUILayout.EndArea();
                GUILayout.EndArea();

                Repaint();

                return;
            }
            
            sliderIndex = (int)GUI.VerticalScrollbar(new Rect(this.position.width - 40f, 0, 14, this.position.height - 140), sliderIndex, 17, 0, maxScrollValue);

            sliderIndex = Mathf.Clamp(sliderIndex, 0, elements.Count - 1);

            for (int i = sliderIndex; i < elements.Count; ++i)
            {
                Element element = elements[i];

                if (element.Tag.IndexOf(searchString, System.StringComparison.OrdinalIgnoreCase) >= 0 && (element.Type == sortMask || sortMask == 0))
                {
                    height = defHeight;
                    GUIContent content = new GUIContent(element.Value);
                    float factor = EditorStyles.textArea.CalcHeight(content, this.position.width - 480) / 17;
                    height += (int)(16 * (factor - 1));
                    
                    GUILayout.BeginArea(new Rect(5, offset + 2, this.position.width - 45, height), EditorStyles.helpBox);

                    if(element.SetType((ElementType)EditorGUI.EnumPopup(new Rect(10, 5.0f, 80, 16), element.Type, EditorStyles.popup)))
                    {
                        LocalizationManager.SetElementType(i, element.Type);

                        LocalizationManager.wasChange = true;
                    }

                    if (element.SetId(EditorGUI.IntField(new Rect(100, 5.0f, 60, 16), element.Id, EditorStyles.textField)))
                    {
                        LocalizationManager.SetElementId(i, element.Id);

                        LocalizationManager.wasChange = true;
                    }

                    if (element.SetTag(GUI.TextField(new Rect(170, 5.0f, 200, 16), element.Tag, EditorStyles.textField)))
                    {
                        LocalizationManager.SetElementTag(i, element.Tag);

                        LocalizationManager.wasChange = true;
                    }

                    if (element.Type == ElementType.text)
                    {
                        if(element.SetValue(GUI.TextArea(new Rect(380, 5.0f, this.position.width - 480, 16 * factor), element.Value, EditorStyles.textArea))) 
                        {
                            LocalizationManager.wasChange = true;
                        }
                    }
                    else if (element.Type == ElementType.sprite)
                    {
                        element.obj = EditorGUI.ObjectField(new Rect(380, 5.0f, this.position.width - 480, 16), element.obj, typeof(Sprite), false);

                        element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                    }
                    else if (element.Type == ElementType.audio)
                    {
                        element.obj = EditorGUI.ObjectField(new Rect(380, 5.0f, this.position.width - 480, 16), element.obj, typeof(AudioSource), false);

                        element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                    }
                    else if (element.Type == ElementType.texture)
                    {
                        element.obj = EditorGUI.ObjectField(new Rect(380, 5.0f, this.position.width - 480, 16), element.obj, typeof(Texture2D), false);

                        element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                    }
                    else if (element.Type == ElementType.material)
                    {
                        element.obj = EditorGUI.ObjectField(new Rect(380, 5.0f, this.position.width - 480, 16), element.obj, typeof(Material), false);

                        element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                    }
                    else if (element.Type == ElementType.prefab)
                    {
                        element.obj = EditorGUI.ObjectField(new Rect(380, 5.0f, this.position.width - 480, 16), element.obj, typeof(GameObject), false);

                        element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                    }
                    else if (element.Type == ElementType.obj)
                    {
                        element.obj = EditorGUI.ObjectField(new Rect(380, 5.0f, this.position.width - 480, 16), element.obj, typeof(Object), false);

                        element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                    }

                    if (GUI.Button(new Rect(this.position.width - 70, 3.5f, 20, 16), LoadIcon("Remove"), bR))
                    {
                        RemoveElement(element);

                        break;
                    }

                    if (GUI.Button(new Rect(this.position.width - 90, 3.5f, 20, 16), LoadIcon("Edit"), bL))
                    {
                        KeyEditorWindow.ShowWindow(element.Id);
                    }
                    
                    GUILayout.EndArea();
                    
                    offset += height;
                }

                if (offset + (defHeight + 2) > availableHeight) break;

                counter++;
            }
            
            GUILayout.EndArea();
            GUILayout.EndArea();
        }
        
        public static List<string> GetLanguagesName()
        {
            List<string> list = new List<string>();
            
            foreach (Language l in LocalizationManager.source.languages)
            {
                list.Add(l.Name);
            }

            return list;
        }
        
        public static void CreateNewElement(ElementType type)
        {
            LocalizationManager.AddElement(type);
        }

        public static void RemoveElement(Element element)
        {
            LocalizationManager.RemoveElement(element);
        }

        public static void AddNewLanguage()
        {
            LanguageWindow.ShowWindow();
        }

        private GUIContent LoadIcon(string name, string tip = "")
        {
            Texture2D texture = LocalizationManager.LoadIcon(name);

            GUIContent content = new GUIContent(texture, tip);

            return content;
        }

        private void SetCurrentLanguage()
        {
            languageIndex = Mathf.Clamp(languageIndex, 0, LocalizationManager.source.languages.Count - 1);

            LocalizationManager.SetCurrent(languageIndex);
        }

        private GUIContent GetSaveIcon()
        {
            if (LocalizationManager.wasChange)
                return LoadIcon("NeedSave", "Save changes");
            else
                return LoadIcon("Save", "Save changes");
        }

        private GUIContent GetDefaultIcon()
        {
            if (LocalizationManager.current == LocalizationManager.GetDefault())
                return LoadIcon("EDefault", "Set as default");
            else
                return LoadIcon("DDefault", "Set as default");
        }

        private void Save()
        {
            LocalizationManager.Save();
        }
    }

    
}


