﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace FLocalization.FEditor
{
    public class ConstantsGeneratorWindow : EditorWindow
    {
        public static void ShowWindow()
        {
            EditorWindow window = GetWindow(typeof(ConstantsGeneratorWindow));

            window.titleContent = new GUIContent("Constants Generator");

            window.minSize = new Vector2(400, 35);
            window.maxSize = new Vector2(400, 35);
        }

        private void OnGUI()
        {
            LocalizationManager.source.keysNamespace = EditorGUILayout.TextField("Namespace", LocalizationManager.source.keysNamespace);

            if (GUILayout.Button("Generate"))
            {
                FileManager.CreateKeys();
            }
        }
    }

}
