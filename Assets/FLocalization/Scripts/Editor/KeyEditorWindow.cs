﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

namespace FLocalization.FEditor
{
    public class KeyEditorWindow : EditorWindow
    {
        private static Vector2 scroolPos;
        
        private static List<Container> elements = new List<Container>();

        public static void ShowWindow(int id)
        {
            EditorWindow window = GetWindow(typeof(KeyEditorWindow));
            
            window.titleContent = new GUIContent("Key Editor");

            window.minSize = new Vector2(400, 400);

            elements = GetElements(id);
        }
        
        private void OnGUI()
        {
            if (elements.Count == 0) return;

            GUIStyle titleStyle = new GUIStyle { alignment = TextAnchor.MiddleCenter, fontStyle = FontStyle.Bold, wordWrap = true, fontSize = 15 };
            GUIStyle textStyle = new GUIStyle { alignment = TextAnchor.MiddleLeft, fontSize = 11 };

            GUILayout.BeginArea(new Rect(10, 10, this.position.width - 20, 37.5f), EditorStyles.helpBox);

            EditorGUI.LabelField(new Rect(10, 10, 40, 16), "Name", textStyle);

            SetElementTag(GUI.TextField(new Rect(50, 11f, this.position.width - 80, 16), elements[0].Element.Tag));

            GUILayout.EndArea();
            
            GUILayout.BeginArea(new Rect(10, 60, this.position.width - 20, this.position.height - 75), EditorStyles.helpBox);

            scroolPos = GUILayout.BeginScrollView(scroolPos, false, true);
            
            int height = 25;

            int offset = 0;

            foreach (Container container in elements)
            {
                Element element = container.Element;

                height = 25;
                GUIContent content = new GUIContent(element.Value);
                float factor = EditorStyles.textArea.CalcHeight(content, this.position.width - 270) / 17;
                height += (int)(16 * (factor - 1));

                GUILayout.Label("", GUILayout.Height(height - 2));
                
                GUILayout.BeginArea(new Rect(5, offset + 2, this.position.width - 50, height), EditorStyles.helpBox);

                EditorGUI.LabelField(new Rect(10, 5, 150, 16), container.Language);

                if (element.Type == ElementType.text)
                {
                    if(element.SetValue(GUI.TextArea(new Rect(160, 5.0f, this.position.width - 220, 16 * factor), element.Value, EditorStyles.textArea))) 
                    {
                        LocalizationManager.wasChange = true;
                    }
                }
                else if (element.Type == ElementType.sprite)
                {
                    element.obj = EditorGUI.ObjectField(new Rect(160, 5.0f, this.position.width - 220, 16), element.obj, typeof(Sprite), false);

                    element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                }
                else if (element.Type == ElementType.audio)
                {
                    element.obj = EditorGUI.ObjectField(new Rect(160, 5.0f, this.position.width - 220, 16), element.obj, typeof(AudioSource), false);

                    element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                }
                else if (element.Type == ElementType.texture)
                {
                    element.obj = EditorGUI.ObjectField(new Rect(160, 5.0f, this.position.width - 220, 16), element.obj, typeof(Texture2D), false);

                    element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                }
                else if (element.Type == ElementType.material)
                {
                    element.obj = EditorGUI.ObjectField(new Rect(160, 5.0f, this.position.width - 220, 16), element.obj, typeof(Material), false);

                    element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                }
                else if (element.Type == ElementType.prefab)
                {
                    element.obj = EditorGUI.ObjectField(new Rect(160, 5.0f, this.position.width - 220, 16), element.obj, typeof(GameObject), false);

                    element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                }
                else if (element.Type == ElementType.obj)
                {
                    element.obj = EditorGUI.ObjectField(new Rect(160, 5.0f, this.position.width - 220, 16), element.obj, typeof(Object), false);

                    element.SetValue(AssetDatabase.GetAssetPath(element.obj));
                }

                GUILayout.EndArea();

                offset += height;
            }

            GUILayout.EndScrollView();

            GUILayout.EndArea();
        }

        private static void SetElementTag(string value)
        {
            for(int i = 0; i < elements.Count; ++i)
            {
                elements[i].Element.Tag = value;
            }

            EditorUtility.SetDirty(LocalizationManager.source);
        }

        private static List<Container> GetElements(int id)
        {
            List<Container> elements = new List<Container>();
            
            foreach (Language language in LocalizationManager.source.languages)
            {
                elements.Add(new Container(language.GetElementById(id), language.Name));
            }

            return elements;
        }

        class Container
        {
            public Element Element;
            public string Language;

            public Container(Element element, string language)
            {
                Element = element;
                Language = language;
            }
        }
    }
}
