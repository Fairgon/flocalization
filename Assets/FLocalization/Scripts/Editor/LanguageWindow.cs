﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

namespace FLocalization.FEditor
{
#if UNITY_EDITOR
    public class LanguageWindow : EditorWindow
    {
        private static Vector2 scroolPos;

        private string searchString = "";

        public static void ShowWindow()
        {
            EditorWindow window = GetWindow(typeof(LanguageWindow));

            window.maxSize = new Vector2(400, 600);
            window.minSize = new Vector2(400, 600);

            window.titleContent = new GUIContent("Languages");

            LanguageManager.LoadLanguages();
        }

        private void OnGUI()
        {
            GUIStyle titleStyle = new GUIStyle { alignment = TextAnchor.MiddleCenter, fontStyle = FontStyle.Bold, wordWrap = true, fontSize = 15 };
            GUIStyle textStyle = new GUIStyle { alignment = TextAnchor.MiddleLeft, fontSize = 12 };

            GUILayout.BeginArea(new Rect(10, 0, this.position.width - 20, 45), EditorStyles.helpBox);

            EditorGUI.LabelField(new Rect(0, 0, this.position.width - 20, 45), "Languages", titleStyle);

            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(10, 45, this.position.width - 20, 30), EditorStyles.helpBox);

            GUI.Label(new Rect(10, 7.5f, 60, 16), "Search:");

            searchString = GUI.TextField(new Rect(70, 7.5f, this.position.width - 100, 16), searchString, 20, EditorStyles.textArea);
            
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(10, 75, this.position.width - 20, this.position.height - 75), EditorStyles.helpBox);

            scroolPos = GUILayout.BeginScrollView(scroolPos, EditorStyles.helpBox);

            int i = 0;
            int height = 30;

            foreach (Language language in LanguageManager.languages)
            {
                if (language.Name.IndexOf(searchString, System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    GUILayout.Label("", GUILayout.Height(height - 2));

                    GUILayout.BeginArea(new Rect(2, 2 + (height * i), this.position.width - 50, height), EditorStyles.helpBox);

                    GUI.Label(new Rect(10, 0, this.position.width, height), language.Name, textStyle);

                    if (GUI.Button(new Rect(this.position.width - 80, 5, 20, 20), GetIcon(language))) Language(language);

                    GUILayout.EndArea();

                    i++;
                }
            }

            GUILayout.EndScrollView();

            GUILayout.EndArea();
        }

        private static Texture2D GetIcon(Language value)
        {
            return LocalizationManager.ContainsLanguage(value.Name) ? LocalizationManager.LoadIcon("Remove") : LocalizationManager.LoadIcon("Add");
        }

        private static void Language(Language language)
        {
            if (LocalizationManager.ContainsLanguage(language.Name))
                LocalizationManager.RemoveLanguage(language.Name);
            else
                LocalizationManager.AddLanguage(language);
        }
    }
#endif
}

