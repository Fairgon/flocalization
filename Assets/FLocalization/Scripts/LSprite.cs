﻿using UnityEngine;
using UnityEngine.UI;

namespace FLocalization
{
    public class LSprite : MonoBehaviour
    {
        public string id;

        private Image image;

        private void Awake()
        {
            Localization.Register(this);
        }

        private void OnEnable()
        {
            UpdateSprite();
        }

        public void UpdateSprite()
        {
            image = GetComponent<Image>();

            image.sprite = Localization.GetSprite(id);
        }

        private void OnDestroy()
        {
            Localization.Unregister(this);
        }

        private void OnValidate()
        {
            name = id;
        }
    }
}
