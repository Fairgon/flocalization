﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Xml;
using System.Linq;

namespace FLocalization.FEditor
{
    public enum ElementType
    {
        text = 1,
        sprite = 2,
        audio = 3,
        texture = 4,
        material = 5,
        prefab = 6,
        obj = 7
    }
#if UNITY_EDITOR
    public class LocalizationManager
    {
        public static Languages source;

        public static Language current;

        public static bool wasChange = false;

        public static Languages GetSource()
        {
            if (source == null) Load();

            return source;
        }

        public static void Load()
        {
            Directory.CreateDirectory("Assets/Resources/FLocalization");

            if(!File.Exists("Assets/Resources/FLocalization/Localization.asset"))
            {
                CreateNew();
            }

            source = AssetDatabase.LoadAssetAtPath<Languages>("Assets/Resources/FLocalization/Localization.asset");
        }
        
        public static void CreateNew()
        {
            Languages languages = new Languages();
            
            AssetDatabase.CreateAsset(languages, "Assets/Resources/FLocalization/Localization.asset");
        }

        public static void SetCurrent(int index)
        {
            if (source.languages.Count == 0) return;

            current = source.languages[index];
        }

        public static bool ContainsLanguage(string name)
        {
            for (int i = 0; i < source.languages.Count; ++i)
            {
                if (source.languages[i].Name == name) return true;
            }

            return false;
        }

        public static void SetDefault()
        {
            if (source.languages.Count == 0) return;

            for (int i = 0; i < source.languages.Count; ++i)
            {
                source.languages[i].IsDefault = false;
            }

            current.IsDefault = true;

            EditorUtility.SetDirty(source);
            wasChange = true;
        }

        public static void SetDefault(int index)
        {
            if (source.languages.Count == 0) return;

            source.languages[0].IsDefault = true;

            EditorUtility.SetDirty(source);
            wasChange = true;
        }

        public static void SetDefault(string name)
        {
            if (source.languages.Count == 0) return;

            for (int i = 0; i < source.languages.Count; ++i)
            {
                source.languages[i].IsDefault = source.languages[i].Name == name;
            }

            EditorUtility.SetDirty(source);
        }

        public static Language GetDefault()
        {
            for (int i = 0; i < source.languages.Count; ++i)
            {
                if (source.languages[i].IsDefault == true) return source.languages[i];
            }

            return null;
        }

        public static void AddLanguage(Language language)
        {
            List<Element> elements = new List<Element>();

            if (GetDefault() != null)
            {
                elements = GetDefault().GetElements();
            }
            
            source.languages.Add(new Language(language, elements));

            if (source.languages.Count == 1)
            {
                SetCurrent(0);
                SetDefault();
            }
            
            EditorUtility.SetDirty(source);
            wasChange = true;
        }

        public static void RemoveLanguage(string name)
        {
            Language language = null;

            for (int i = 0; i < source.languages.Count; ++i)
            {
                if (source.languages[i].Name == name)
                {
                    language = source.languages[i];
                }
            }

            if (language != null) source.languages.Remove(language);

            if (language.IsDefault)
            {
                SetDefault();
            }

            EditorUtility.SetDirty(source);
            wasChange = true;
        }

        public static List<Element> GetElements()
        {
            if (current == null) return new List<Element>();

            return current.GetElements();
        }

        public static List<Element> GetElements(string languageCode)
        {
            foreach(Language language in source.languages)
            {
                if(language.Code == languageCode)
                {
                    return language.GetElements();
                }
            }

            return new List<Element>();
        }

        public static Element GetElementById(int id)
        {
            Element[] elements = current.GetElements().ToArray();

            for (int i = 0; i < elements.Length; ++i)
            {
                if (elements[i].Id == id) return elements[i];
            }

            return null;
        }

        public static void AddElement(string tag, string value, ElementType type)
        {
            Element element = new Element(0, tag, value, type);

            element.Id = GetNewId();

            for (int i = 0; i < source.languages.Count; ++i)
            {
                source.languages[i].AddElement(new Element(element));
            }

            EditorUtility.SetDirty(source);
            wasChange = true;
        }

        public static void AddElement(ElementType type)
        {
            Element element = new Element(0, "new_" + type, "", type);

            element.Id = GetNewId();
            element.Tag += "_" + element.Id;

            for (int i = 0; i < source.languages.Count; ++i)
            {
                source.languages[i].AddElement(new Element(element));
            }

            EditorUtility.SetDirty(source);
            wasChange = true;
        }

        public static void RemoveElement(Element element)
        {
            for (int i = 0; i < source.languages.Count; ++i)
            {
                source.languages[i].RemoveElement(element.Hash);
            }

            EditorUtility.SetDirty(source);
            wasChange = true;
        }

        public static void SetElementId(int index, int id)
        {
            for (int i = 0; i < source.languages.Count; ++i)
            {
                source.languages[i].SetElementIdByIndex(index, id);
            }

            EditorUtility.SetDirty(source);
        }

        public static void SetElementTag(int index, string tag)
        {
            for (int i = 0; i < source.languages.Count; ++i)
            {
                source.languages[i].SetElementTagByIndex(index, tag);
            }

            EditorUtility.SetDirty(source);
        }

        public static void SetElementType(int index, ElementType type)
        {
            for (int i = 0; i < source.languages.Count; ++i)
            {
                source.languages[i].SetElementTypeByIndex(index, type);
            }

            EditorUtility.SetDirty(source);
        }

        private static int GetNewId()
        {
            List<Element> elements = current.GetElements();

            int id = 0;

            for(int i = 0; i < elements.Count; ++i)
            {
                if (elements[i].Id > id) id = elements[i].Id;
            }

            return id + 1;
        }

        public static Texture2D LoadIcon(string name)
        {
            Texture2D texture = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/FLocalization/Icons/" + name + ".png");

            return texture;
        }

        public static void Save()
        {
            FileManager.SaveKeys();

            AssetDatabase.SaveAssets();

            wasChange = false;
        }
    }
#endif
}
    

