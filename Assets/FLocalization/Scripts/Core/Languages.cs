﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace FLocalization.FEditor
{
    [System.Serializable]
    public class Languages : ScriptableObject
    {
        [SerializeField]
        public List<Language> languages = new List<Language>();

        [SerializeField]
        public string keysPath = "";

        [SerializeField]
        public string keysNamespace = "";

        [SerializeField]
        public string keysClassName = "";
    }

    [System.Serializable]
    public class Element
    {
        public int Id;

        [SerializeField]
        private int HashCode;

        public string Tag;
        public string Value;
        
        public ElementType Type;
        
        public Object obj;

        public Element(int id, string tag, string value, ElementType type)
        {
            Id = id;

            Tag = tag;

            Value = value;

            Type = type;

            HashCode = System.DateTime.Now.GetHashCode();
        }

        public Element(Element element)
        {
            Id = element.Id;

            Tag = element.Tag;

            Value = element.Value;

            Type = element.Type;

            if(element.HashCode != 0) HashCode = element.HashCode;
            else HashCode = System.DateTime.Now.GetHashCode();
        }

        public Element()
        {

        }

        public int Hash
        {
            get
            {
                return HashCode;
            }
        }

        public bool Change(int id, string tag, ElementType type)
        {
            bool value = Id != id || Tag != tag || Type != type;

            Id = id;
            Tag = tag;
            Type = type;

            return value;
        }

        public bool SetValue(string value)
        {
            bool wasChange = Value != value;
            
            Value = value;

            return wasChange;
        }

        public bool SetTag(string tag)
        {
            bool wasChange = Tag != tag;

            Tag = tag;

            return wasChange;
        }

        public bool SetId(int id)
        {
            bool wasChange = Id != id;

            Id = id;

            return wasChange;
        }

        public bool SetType(ElementType type)
        {
            bool wasChange = Type != type;

            Type = type;

            return wasChange;
        }
    }

    [System.Serializable]
    public class Language
    {
        public string Code;
        public string Name;
        
        public bool IsDefault = false;
        
        [SerializeField]
        private List<Element> Elements = new List<Element>();

        public Language(string code, string name, bool isDefault = false)
        {
            Code = code;

            Name = name;

            IsDefault = isDefault;
        }

        public Language(Language language, List<Element> elements)
        {
            Code = language.Code;
            Name = language.Name;
            IsDefault = language.IsDefault;

            Elements = new List<Element>();

            foreach (Element element in elements)
            {
                Elements.Add(new Element(element));
            }
        }

        public List<Element> GetElements()
        {
            return Elements;
        }

        public Element GetElementByTag(string tagName)
        {
            for (int i = 0; i < Elements.Count; ++i)
            {
                if (Elements[i].Tag == tagName) return Elements[i];
            }

            return new Element();
        }

        public Element GetElementById(int id)
        {
            for (int i = 0; i < Elements.Count; ++i)
            {
                if (Elements[i].Id == id) return Elements[i];
            }

            return new Element();
        }

        public void AddElement(Element element)
        {
            Elements.Add(element);
        }
        
        public void RemoveElement(int hashCode)
        {
            foreach(Element element in Elements)
            {
                if (element.Hash == hashCode)
                {
                    Elements.Remove(element);
                    break;
                }
            }
        }

        public void ChangeElement(Element element)
        {
            bool wasChange = false;

            for (int i = 0; i < Elements.Count; ++i)
            {
                if (Elements[i].Hash == element.Hash)
                {
                    wasChange |= Elements[i].Change(element.Id, element.Tag, element.Type);

                    break;
                }
            }

#if UNITY_EDITOR
            if (wasChange)
            {
                LocalizationManager.wasChange = wasChange;

                EditorUtility.SetDirty(LocalizationManager.source);
            }
#endif
        }

        public void SetElementIdByIndex(int index, int id)
        {
            Elements[index].SetId(id);
        }

        public void SetElementTagByIndex(int index, string tag)
        {
            Elements[index].SetTag(tag);
        }

        public void SetElementTypeByIndex(int index, ElementType type)
        {
            Elements[index].SetType(type);
        }
    }

}
