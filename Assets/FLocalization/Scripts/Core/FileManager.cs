﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace FLocalization.FEditor
{
#if UNITY_EDITOR
    public static class FileManager
    {
        [SerializeField]
        private static string lastPath = "";

        public static void CreateKeys()
        {
            string path = EditorUtility.SaveFilePanel("Create constants", Application.dataPath, "FKeys", "cs");

            if (path == "") return;

            string name = path.Substring(path.LastIndexOf('/') + 1).Replace(".cs", "");

            LocalizationManager.source.keysClassName = name;
            LocalizationManager.source.keysPath = path;

            List<Element> elements = LocalizationManager.current.GetElements();

            string str = "";

            if (LocalizationManager.source.keysNamespace != "")
            {
                str = "namespace " + LocalizationManager.source.keysNamespace;
                str += "\n{\n";
            }

            str += "public class " + name;
            str += "\n{\n";

            foreach(ElementType type in System.Enum.GetValues(typeof(ElementType)))
            {
                WriteElementsByType(ref str, type, elements);
            }

            str += "}\n";

            if (LocalizationManager.source.keysNamespace != "") str += "}\n";

            File.WriteAllText(path, str);

            AssetDatabase.Refresh();

            EditorUtility.SetDirty(LocalizationManager.source);
        }

        private static void WriteElementsByType(ref string str, ElementType type, List<Element> elements)
        {
            string name = type.ToString();
            name = name[0].ToString().ToUpper() + name.Substring(1);
            
            str += "\tpublic class " + name;
            str += "\n\t{\n";

            foreach (Element element in elements)
            {
                if (element.Type == type)
                {
                    str += "\t\tpublic const int " + element.Tag + " = " + element.Id + ";\n";
                }
            }

            str += "\n\t}\n";
        }

        public static void SaveKeys()
        {
            string path = LocalizationManager.source.keysPath;

            if (path == "" || path == null) return;

            List<Element> elements = LocalizationManager.current.GetElements();

            string str = "";
            
            if (LocalizationManager.source.keysNamespace != "")
            {
                str = "namespace " + LocalizationManager.source.keysNamespace;
                str += "\n{\n";
            }

            str += "public class " + LocalizationManager.source.keysClassName;
            str += "\n{\n";

            foreach (ElementType type in System.Enum.GetValues(typeof(ElementType)))
            {
                WriteElementsByType(ref str, type, elements);
            }

            str += "}\n";

            if (LocalizationManager.source.keysNamespace != "") str += "}\n";
            
            File.WriteAllText(path, str);

            AssetDatabase.Refresh();
        }

        public static void Export()
        {
            if (lastPath == "") lastPath = Application.dataPath;

            string name = LocalizationManager.current.Code.ToUpper() + "_Localization.txt";

            string path = EditorUtility.SaveFilePanel("Export", lastPath, name, "*.txt;*.xml");

            lastPath = path;

            if (path.Contains(".xml"))
            {
                ExportToXML(path);
            }
            else if(path.Contains(".txt"))
            {
                ExportToTXT(path);
            }
        }

        public static void Import()
        {
            if (lastPath == "") lastPath = Application.dataPath;

            string path = EditorUtility.OpenFilePanelWithFilters("Export", lastPath, new string[] { "TXT", "txt", "XML", "xml" });

            lastPath = path;

            if (!File.Exists(path)) return;
            
            if (path.Contains(".xml"))
            {
                ImportFromXML(path);
            }
            else if (path.Contains(".txt"))
            {
                ImportFromTXT(path);
            }
        }

        private static void ExportToXML(string path)
        {
            XmlWriter writer = XmlWriter.Create(path, new XmlWriterSettings { Indent = true });
            
            writer.WriteStartDocument();
            writer.WriteStartElement("Localization");

            writer.WriteAttributeString("Language", LocalizationManager.current.Code);

            List<Element> elements = LocalizationManager.GetElements();

            foreach(Element element in elements)
            {
                writer.WriteStartElement("Element");
                
                writer.WriteAttributeString("name", element.Tag.ToString());

                writer.WriteString(element.Value);

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();

            writer.Close();
        }

        private static void ImportFromXML(string path)
        {
            string text = File.ReadAllText(path);

            XmlDocument document = new XmlDocument();

            document.LoadXml(text);

            XmlElement n = (XmlElement)document.SelectSingleNode("Localization");

            string languageCode = n.GetAttribute("Language");

            Dictionary<string, string> pairs = new Dictionary<string, string>();

            foreach (XmlElement node in document.SelectNodes("Localization/Element"))
            {
                pairs.Add(node.GetAttribute("name"), node.InnerText);
            }

            List<Element> elements = LocalizationManager.GetElements(languageCode);

            foreach (string key in pairs.Keys)
            {
                bool exist = false;

                foreach (Element element in elements)
                {
                    if (element.Tag == key)
                    {
                        exist = true;

                        element.Value = pairs[key];

                        break;
                    }
                }

                if (!exist)
                {
                    LocalizationManager.AddElement(key, pairs[key], ElementType.text);
                }
            }
        }

        private static void ExportToTXT(string path)
        {
            List<Element> elements = LocalizationManager.current.GetElements();

            string str = "";

            str += "Language=" + LocalizationManager.current.Code + "\n";

            foreach (Element element in elements)
            {
                str += element.Tag + "=" + element.Value + "\n";
            }
            
            File.WriteAllText(path, str);
        }

        private static void ImportFromTXT(string path)
        {
            string text = File.ReadAllText(path);
            
            Dictionary<string, string> pairs = new Dictionary<string, string>();

            foreach(string pair in text.Split('\n'))
            {
                string[] mas = pair.Split('=');

                if (!pair.Contains("=")) continue;

                pairs.Add(mas[0], mas[1]);
            }

            string languageCode = pairs["Language"];

            pairs.Remove("Language");

            List<Element> elements = LocalizationManager.GetElements(languageCode);
            
            foreach(string key in pairs.Keys)
            {
                bool exist = false;

                foreach(Element element in elements)
                {
                    if(element.Tag == key)
                    {
                        exist = true;

                        element.Value = pairs[key];

                        break;
                    }  
                }

                if(!exist)
                {
                    LocalizationManager.AddElement(key, pairs[key], ElementType.text);
                }
            } 
        }
    }
#endif
}

