﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using UnityEditor;

namespace FLocalization.FEditor
{
#if UNITY_EDITOR
    public class LanguageManager
    {
        public static List<Language> languages = new List<Language>();

        public static void LoadLanguages()
        {
            TextAsset source = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/FLocalization/Languages.xml");

            List<Language> list = new List<Language>();

            XmlDocument doc = new XmlDocument();

            doc.LoadXml(source.ToString());

            foreach (XmlElement node in doc.SelectNodes("Languages/Language"))
            {
                list.Add(new Language(node.GetAttribute("code"), node.GetAttribute("name")));
            }

            languages = new List<Language>(list);
        }

        public static void CreateLanguage(string code, string name)
        {

        }
    }
#endif
}

