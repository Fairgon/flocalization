﻿using UnityEngine;
using FLocalization;

public class Manager : MonoBehaviour
{
    public void SetLanguage(string value)
    {
        Localization.Language = value;

        Localization.Translate();
    }
}
