namespace FLocalization
{
public class FKeys
{
	public class Text
	{
		public const int Start = 1;
		public const int Exit = 2;
		public const int Settings = 3;
		public const int Russian = 4;
		public const int English = 5;

	}
	public class Sprite
	{
		public const int Language_Sprite = 6;

	}
	public class Audio
	{

	}
	public class Texture
	{

	}
	public class Material
	{
		public const int Language_Cub = 7;

	}
	public class Prefab
	{

	}
	public class Obj
	{

	}
}
}
